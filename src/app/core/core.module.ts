import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {AppRoutingModule} from '../app-routing.module';
import {HeaderComponent} from './header/header.component';

@NgModule({
    imports: [
        CommonModule,
        AppRoutingModule,
        FormsModule,
        ReactiveFormsModule
    ],
    declarations: [
        HeaderComponent
    ],
    exports: [
        HeaderComponent,
        AppRoutingModule
    ],
    providers: []
})
export class CoreModule {
}
