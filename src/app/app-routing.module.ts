import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
    {
        path: '',
        redirectTo: '/to-do-list',
        pathMatch: 'full'
    },
    {
        path: 'to-do-list',
        loadChildren: './to-do-list/to-do-list.module#ToDoListModule'
    }

];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
