import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {ToDoListRoutingModule} from './to-do-list-routing.module';
import {ToDoListComponent} from './to-do-list.component';
import {ToDoListService} from './to-do-list.service';
import {ToDoEditComponent} from './to-do-edit/to-do-edit.component';

@NgModule({
    imports: [
        CommonModule,
        ToDoListRoutingModule,
        FormsModule,
        ReactiveFormsModule
    ],
    declarations: [
        ToDoListComponent,
        ToDoEditComponent
    ],
    providers: [
        ToDoListService
    ]
})
export class ToDoListModule {
}
