import {Injectable} from '@angular/core';
import {Subject, BehaviorSubject} from 'rxjs';
import _ from 'lodash';
import {ToDoItemModel} from '../shared/to-do-item.model';

@Injectable()
export class ToDoListService {
    private toDoItems: ToDoItemModel[] = [
        new ToDoItemModel(1, 'first', 'PENDING'),
        new ToDoItemModel(2, 'second', 'DONE'),
        new ToDoItemModel(3, 'third', 'ARCHIVED')
    ];
    public toDoItemsChange = new BehaviorSubject<ToDoItemModel[]>(this.toDoItems);

    constructor() {
    }

    /**
     * @param {ToDoItemModel} toDoItem
     * @return {ToDoListService}
     */
    addItem(toDoItem: ToDoItemModel): ToDoListService {
        _.orderBy(this.toDoItems.push(toDoItem), ['id'], ['asc']);
        this.toDoItemsChange.next(this.toDoItems.slice());
        return this;
    }

    /**
     * @param {number} id
     * @return {ToDoItemModel}
     */
    getItem(id: number): ToDoItemModel {
        return _.find(this.toDoItems, (item: ToDoItemModel) => {
            return item.id === id;
        });
    }

    /**
     * @return {ToDoItemModel[]}
     */
    getAllItems(): ToDoItemModel[] {
        return this.toDoItems;
    }

    /**
     * @param {string} status
     */
    getItemByStatus(status: string): void {
        this.toDoItemsChange.next(
            _.filter(this.toDoItems, (item: ToDoItemModel) => {
                return item.status.toLowerCase() === status;
            })
        );
    }

    getItems() {
        this.toDoItemsChange.next(_.orderBy(this.toDoItems.slice(), ['id'], ['asc']));
    }

    updateItem(id: number, updatedItem: ToDoItemModel) {
        this.toDoItems = _.filter(this.toDoItems, (item: ToDoItemModel) => {
            return item.id !== id;
        });
        this.toDoItems.push(updatedItem);
        this.toDoItemsChange.next(_.orderBy(this.toDoItems.slice(), ['id'], ['asc']));
    }

    deleteRecipe(id: number) {
        this.toDoItems = _.filter(this.toDoItems, (item: ToDoItemModel) => {
            return item.id !== id;
        });
        this.toDoItemsChange.next(this.toDoItems.slice());
    }

    search(value: string) {
        this.toDoItemsChange.next(_.orderBy(value ? _.filter(this.toDoItems, (item: ToDoItemModel) => {
            return item.title.indexOf(value) > -1;
        }) : this.toDoItems, ['id'], ['asc']));
    }
}
