import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Subscription} from 'rxjs';

import {ToDoListService} from '../to-do-list.service';
import {ToDoItemModel} from '../../shared/to-do-item.model';

@Component({
    selector: 'app-to-do-edit',
    templateUrl: './to-do-edit.component.html',
    styleUrls: ['./to-do-edit.component.css']
})
export class ToDoEditComponent implements OnInit, OnDestroy {

    public listItem: ToDoItemModel;
    private routerSubscription: Subscription;
    private id: number;
    private editMode = false;
    private item: ToDoItemModel = null;
    itemForm: FormGroup;

    constructor(private toDoListService: ToDoListService,
                private activatedRoute: ActivatedRoute,
                private router: Router) {
    }

    ngOnInit() {
        this.routerSubscription = this.activatedRoute.params
            .subscribe((params: Params) => {
                this.id = +params.id;
                this.editMode = params['id'] != null;
                this.item = this.toDoListService.getItem(this.id);
                if (this.editMode && !this.item) {
                    this.onCancel();
                    return;
                }
                this.initForm();
            });
    }

    private initForm() {
        let itemTitle = '',
            itemStatus = '';
        if (this.editMode) {
            itemTitle = this.item.title;
            itemStatus = this.item.status;
        }
        this.itemForm = new FormGroup({
            'title': new FormControl(itemTitle, Validators.required),
            'status': new FormControl(itemStatus, Validators.required)
        });
    }

    onSubmit() {
        const newItem = new ToDoItemModel(
            Math.round(Math.random() * 100) + 1,
            this.itemForm.value['title'],
            this.itemForm.value['status'],
        );
        if (this.editMode) {
            this.itemForm.value.id = this.id;
            this.toDoListService.updateItem(
                this.id,
                new ToDoItemModel(this.itemForm.value.id, this.itemForm.value.title, this.itemForm.value.status)
            );
        } else {
            this.toDoListService.addItem(newItem);
        }
        this.onCancel();
    }

    onCancel() {
        this.router.navigate(['../../'], {relativeTo: this.activatedRoute});
    }

    onDeleteItem() {
        this.toDoListService.deleteRecipe(this.id);
        this.onCancel();
    }

    ngOnDestroy() {
        this.routerSubscription.unsubscribe();
    }
}
