import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {Subscription} from 'rxjs';

import {ToDoListService} from './to-do-list.service';
import {ToDoItemModel} from '../shared/to-do-item.model';

@Component({
    selector: 'app-to-do-list',
    templateUrl: './to-do-list.component.html',
    styleUrls: ['./to-do-list.component.css']
})
export class ToDoListComponent implements OnInit, OnDestroy {

    public listItems: ToDoItemModel[] = [];
    private toDoListSubscription: Subscription;
    private routerSubscription: Subscription;

    constructor(private toDoListService: ToDoListService,
                private activatedRoute: ActivatedRoute,
                private router: Router) {
    }

    ngOnInit() {
        this.toDoListSubscription = this.toDoListService.toDoItemsChange
            .subscribe((items: ToDoItemModel[]) => {
                this.listItems = items;
            });
        this.routerSubscription = this.activatedRoute.params
            .subscribe((params: Params) => {
                if (Object.keys(params).length > 0) {
                    this.toDoListService.getItemByStatus(params.status);
                } else {
                    this.toDoListService.getItems();
                }
            });
    }

    editItem(item: ToDoItemModel) {
        this.routerSubscription = this.activatedRoute.params
            .subscribe((params: Params) => {
                if (Object.keys(params).length > 0) {
                    this.router.navigate(['../edit', item.id], {relativeTo: this.activatedRoute});
                } else {
                    this.router.navigate(['edit', item.id], {relativeTo: this.activatedRoute});
                }
            });
    }

    onNavigateToAddItem() {
        this.routerSubscription = this.activatedRoute.params
            .subscribe((params: Params) => {
                if (Object.keys(params).length > 0) {
                    this.router.navigate(['../add'], {relativeTo: this.activatedRoute});
                } else {
                    this.router.navigate(['add'], {relativeTo: this.activatedRoute});
                }
            });
    }

    ngOnDestroy() {
        this.toDoListSubscription.unsubscribe();
        this.routerSubscription.unsubscribe();
    }

    onChangeSearchText(searchInput: HTMLInputElement) {
        this.toDoListService.search(searchInput.value);
    }
}
