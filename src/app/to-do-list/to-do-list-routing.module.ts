import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {ToDoListComponent} from './to-do-list.component';
import {ToDoEditComponent} from './to-do-edit/to-do-edit.component';

const routes: Routes = [
    {
        path: '', component: ToDoListComponent
    },
    {
        path: 'add',
        component: ToDoEditComponent
    },
    {
        path: 'edit/:id', component: ToDoEditComponent
    },
    {
        path: ':status', component: ToDoListComponent,
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ToDoListRoutingModule {
}
