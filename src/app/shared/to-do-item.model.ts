export class ToDoItemModel {
    public id: number;
    public title: string;
    public status: string;

    constructor(id: number, title: string, status: string) {
        this.id = id;
        this.title = title;
        this.status = status;
    }
}
